output "password" {
  value = random_password.admin_password.result
}

output "ip" {
  value = azurerm_public_ip.ip.ip_address
}