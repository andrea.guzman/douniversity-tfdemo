# Generate and store a random admin_password
resource "random_password" "admin_password" {
  length           = 16
  min_upper        = 1
  min_lower        = 1
  min_numeric      = 1
  min_special      = 1
  special          = true
  override_special = "_%@"
}

resource "random_string" "prefix" {
  length  = 12
  special = false
}