provider "azurerm" {
  features {}
  # subscription_id = var.subscription_id
  # client_id       = var.client_id
  # client_secret   = var.client_secret
  # tenant_id       = var.tenant_id
}

terraform {
  backend "azurerm" {
    resource_group_name  = "monserrat-guzman"
    storage_account_name = "monseguzmanbackend"
    container_name       = "tfstate"
    key                  = "terraform.tfstate"
    # subscription_id      = var.subscription_id
  }
  required_providers {
    azurerm = "~> 2.0"
  }
}